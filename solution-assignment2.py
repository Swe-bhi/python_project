
def solution(input_list):
    """
    Sorts a given list of elevator versions in the ascending order of their major, minor and revision numbers.

    Arguments used (Args):
    input_list: The expected list of which the elevator versions which is represented as strings.

    Returns:
    list: Sorted list of the elevator versions.

    """
    if not all(isinstance(version, str) for version in input_list):
        raise ValueError("All elements in the input_list must be strings.") #Raises and catches an error when the input list is not of DataType string

    # Custom sorting function
    def version_sort_key(version):
        """
        This is a helper function to get the sorting key from a version string without using any of the prebuilt functions of Python like sorted, len etc.

        Arguments (Args):
        version (str) : A version string.

        Return:
        list: This returns a list of strings and integers that are representing the version parts.

        """
        parts = version.split(".")   # Splits the version string into major, minor, and revision parts.
        
        parts = [int(part) if part.isdigit() else part for part in parts] # Converts each part to an integer, filling missing parts with zeros.
        return parts

    # Implement bubble sort algorithm without using len, thereby substituting the prebuilt functions of len, sorted.
    n = 0
    for _v in input_list:
        n += 1

    i = 0
    while i < n:
        j = 0
        while j < n - i - 1:
            if version_sort_key(input_list[j]) > version_sort_key(input_list[j + 1]):  # Swap elements if they are in the wrong order
                
                input_list[j], input_list[j + 1] = input_list[j + 1], input_list[j]
            j += 1
        i += 1
    return input_list  #returns the appended list.

# Example test cases
if __name__ == "__main__":  #Sample testcase, more in verification script.
    
    input_list = ["1.11", "2.0.0", "1.2", "2", "0.1", "1.2.1", "1.1.1", "2.0"]
    print ("Input List", input_list)
    result = solution(input_list)
    print ("ResultList", result)
    wrongList = ', '.join(result)
    rightList = ', '.join(result).replace(', ', '", "')
    print("Wrong", '["%s"]' % " ".join([char.strip() for char in wrongList.split("\n") if char]))
    print("Right", '["%s"]' % " ".join([char.strip() for char in rightList.split("\n") if char]))
