
def solution(input_string):
    """
    Expected return value is a string
    """

    """
    The below block of code converts an input string (which is encrypted intentionally by the minions) to a readable string using a simple algorithm.
    The function below iterates over each character in the given input string and decodes based on the set rules :

    1. If the character is of a lowercase i.e 'a'.....'z' then it is replaced by the lowercase character which is symmetrically opposite to it e.g 'a' will become'z', 'b' will become 'y'
    2. Any other character apart from the lower case character i.e A...Z, punctuations and numbers are left untouched.
    3. Since list and dict comprehensions aren't allowed to be used a simple while loop is implemented here which iterates over each charcter and replaces the original letter with the intended letter and returns the encrypted string in a readable format.
    
    The main parameter used for this is:
    input_string : The encrypted string which needs to decrypted.

    The below of code returns:
    str : which is nothing but the decoded string.

    What are the exceptions that are raised in this?
    1. ValueError : Raised when the input_string is 'Empty' or 'exit'
    2. TypeError : Raised when the data type of the input string is not a string.

    Example:

    solution("wvxibkgrlm")
    'decryption"

    """
    decoded_string = ""   #Initialising an empty string.
    for char in input_string: #Iterates over each character and replaces with the original letter.
        if 'a' <= char <= 'z':
            decoded_string += chr(ord('z') - (ord(char) - ord('a')))  #String getting appended after replacing each character one by one.
        else:
            decoded_string += char
    return decoded_string #String returned after input_string is decoded.
    
def input_value():
    while True:
        input_string = "wrw blf hvv ozhg mrtsg'h vkrhlwv?" #encrypted string to be decoded.
        
        if len(input_string) == 0:
            raise ValueError ("Sorry, blank inputs are not allowed") #ValueError raised to catch blank inputs.
        if type(input_string) != str:
            raise TypeError ("Not an accepted data type") #TypeError raised to catch any inputs other than data type str.
        
            break
        output_text = solution(input_string) #Storing the decoded string in a new variable.
        print("Output Text: ", output_text) #Printing the decoded string-
        break
if __name__ == "__main__": #Calling the main method.
    print("Commander Lambda counts on you!") 
    try:
        input_value()
    except ValueError:                        #Prints the below error message when the code encounters a ValueError.
       print("Error: Input must contain atleast one character")
    except TypeError:                         #Prints the below error message when the code encounters a TypeError.  
        print("Error : Please enter only alphabets")
    
