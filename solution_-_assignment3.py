import string

def solution(filename):

    """
    This function is used for analysing a given file, getting and storing the values and returning the same successfully once the
    code executes successfully based on:
    1. Word lengths : Both Shortest and Longest
    2. Total counts of each letter.
    3. Symbols and punctuations.
    Args taken: The exact filepath of the textfile to be analysed
    Return: A dict is returned with the below keys (values are stored and returned based on the above metrics)
    shortest_words, longest_words, letter_counts, word_counts and line_counts
    """
    
    small_words = []                            # Initializing the variables for metrics.
    character_counts = {}
    long_words = []
    countof_words = 0
    countof_lines = 0

    with open(filename, 'r') as file:           # Opens the file in the read mode and stores the contents in a variable..
        details = file.readlines()

    for line in details:                        # Loop through the file content and splits the lines into words.
        countof_lines += 1

        words = line.split()                    
        
        for word in words:                      # Loops through the words and identifies the length of the words.
            countof_words += 1
            word=word.replace('.', '')
            # Updates the small and long word lists.
            if not small_words or len(word) < len(small_words[0]):
                small_words = [word]
            elif len(word) == len(small_words[0]):
                small_words.append(word)

            if not long_words or len(word) > len(long_words[0]):
                long_words = [word]
            elif len(word) == len(long_words[0]):
                long_words.append(word)

            for char in word:                                       # Updates the letter counts.
                if char.isalnum() or char in string.punctuation:
                    character_counts[char] = character_counts.get(char, 0) + 1

    return {                                                
        "shortest_words": small_words,
        "letter_counts": character_counts,
        "longest_words": long_words,
        "word_count": countof_words,
        "line_count": countof_lines
    }

